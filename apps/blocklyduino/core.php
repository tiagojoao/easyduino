<?php

class Shell {

  private static $_debug               = 0;
  private static $_dir_path_git        = ".";
  private static $_dir_name_git        = 'ino/';
  private static $_url_git             = 'git://github.com/amperka/ino.git';
  private static $_cmd_git             = 'git clone git://github.com/amperka/ino.git';
  private static $_return_code         = array();
  private static $_port                = 'default';
  private static $_model               = 'uno';

  private static function throw_json($string = '')
  {
    $error = array();
    $error['error'] = 1;
    $error['msg'] = $string;
    echo json_encode($error);
  }

  private static function output($string = '')
  {
    $success = array();
    $success['error'] = 0;
    $success['msg'] = $string;
    echo json_encode($success);
  }

  private static function create_makefile()
  {
    //$_return_code = self::$_return_code[];
    //if ( self::$_debug == 1 ) echo 'generating makefile in ' . self::$_dir_name_git . '.<br />';
    $br = PHP_EOL;
    $makefile  = "DESTDIR=/" . $br;
    $makefile .= "PREFIX=/usr/local" . $br;
    $makefile .= "all:" . $br;
    $makefile .= "\t@# do nothing yet" . $br;
    $makefile .= "doc:" . $br;
    $makefile .= "\t$(MAKE) -f doc/Makefile html" . $br;
    $makefile .= "install:" . $br;
    $makefile .= "\tenv python setup.py install --root $(DESTDIR) --prefix $(PREFIX) --exec-prefix $(PREFIX)" . $br;
    $makefile .= ".PHONY : doc" . $br;
    $makefile .= ".PHONY : install" . $br;
    self::$_return_code = file_put_contents(self::$_dir_name_git . 'makefile.txt', $makefile, FILE_APPEND);
    /*if ( $_return_code != false && self::$_debug == 1 )
    {
      echo 'makefile createad sucessfully.<br />';
    } elseif ( self::$_debug == 1 ) {
      self::throw_json( 'could not create makefile.<br />', self::$_return_code[] );
    }*/
  }

  private static function configure()
  {
    if ( isset( $_POST['model'] ) )
    {
      self::$_model = $_POST['model'];
    }
    if ( isset( $_POST['port'] ) )
    {
      self::$_port = $_POST['port'];
    }
  }

  private static function create()
  {
    if ( isset( $_POST['code'] ) )
    {
	      exec('rm -rf ino && mkdir ino && cd ino && ino init -t blink', &self::$_return_code);
        exec('echo "' . $_POST['code'] . '" > ino/src/sketch.ino', &self::$_return_code);
    } else {
      self::throw_json('Please provide some code to compile.');
    }

    //exec('rm -rf isp && mkdir isp && cd isp && ino init -t blink && rm src/sketch.ino && cp ../libs/ISP/ArduinoISP.ino src/sketch.ino && ino build -m ATmega328 && upload -m ATmega328');

  }
  private static function build()
  {
    exec('cd ino && ino build', &self::$_return_code);
  }
  private static function upload()
  {
    exec('cd ino && ino upload', &self::$_return_code);
  }
  private static function serial()
  {
    exec('cd ino && ino serial', &self::$_return_code);
  }

  public static function execute()
  {
    self::create_makefile();
    self::configure();
    self::create();
    self::build();
    self::upload();
    self::serial();
    self::output('Compilation sucessfully');
  }

}

Shell::execute();
