/**
 * @fileoverview Helper functions for generating Arduino blocks.
 * @author inunomaduro@email.com Nuno Maduro
 */
'use strict';

//define blocks
if (!Blockly.Language) Blockly.Language = {};


//define temperature alert max block
Blockly.Language.temperature_alert_max = {
  category: 'RFM12b',
  helpUrl: 'http://nunomaduro.com',
  init: function() {
    this.setColour(0);
    this.appendValueInput("TEMP")
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendTitle("Alert when temp is bigger then")
        .appendTitle(new Blockly.FieldTextInput("34"), "TEMP_MAX");
    this.setTooltip('');
  }
};

//define temperature alert min block
Blockly.Language.temperature_alert_min = {
  category: 'RFM12b',
  helpUrl: 'http://nunomaduro.com',
  init: function() {
    this.setColour(225);
    this.appendValueInput("TEMP")
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendTitle("Alert when temp is smaller then")
        .appendTitle(new Blockly.FieldTextInput("34"), "TEMP_MIN");
    this.setTooltip('');
  }
};



// define generators
Blockly.Arduino.temperature_alert_max = function() {
  // Library Definition
  //Blockly.Arduino.definitions_['define_seriallcd'] = '#include <SerialLCD.h>\n';
  var temp = Blockly.Arduino.valueToCode(this, 'TEMP',
      Blockly.Arduino.ORDER_UNARY_POSTFIX) || '\'\'';
  var temp_max = this.getTitleValue('TEMP_MAX');
  var code = 'temp = ' + temp;
  code = 'Serial.print("DEBUG: Current temperature is ' + temp + '");\n';
  code += 'if (temp > ' + temp_max + ') { \n';
  code += 'Serial.print("ALERT: Temperature is bigger then ' + temp_max + '");\n';
  code += '}'
  return [code];
};


// define generators
Blockly.Arduino.temperature_alert_min = function() {
  // Library Definition
  //Blockly.Arduino.definitions_['define_seriallcd'] = '#include <SerialLCD.h>\n';
  var temp = Blockly.Arduino.valueToCode(this, 'TEMP',
      Blockly.Arduino.ORDER_UNARY_POSTFIX) || '\'\'';
  var temp_min = this.getTitleValue('TEMP_MIN');
  var code = 'temp = ' + temp;
  code = 'Serial.print("DEBUG: Current temperature is ' + temp + '");\n';
  code += 'if (temp < ' + temp_min + ') { \n';
  code += '\t Serial.print("ALERT: Temperature is smaler then ' + temp_min + '");\n';
  code += '}'
  return [code];
};