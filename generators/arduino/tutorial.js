/**
 * @fileoverview Helper functions for generating Arduino blocks.
 * @description Some 
 * @author inunomaduro@email.com Nuno Maduro
 */




'use strict';
//define blocks
if (!Blockly.Language) Blockly.Language = {};


Blockly.Language.led_blink = {
  category: 'Led interno',
  init: function() {
      trigger_click('#led_blink')
      var xml = '<xml xmlns="http://www.w3.org/1999/xhtml">  <block type="inout_buildin_led" x="0" y="0">    <title name="STAT">HIGH</title>    <next>      <block type="base_delay" inline="true">        <value name="DELAY_TIME">          <block type="math_number">            <title name="NUM">200</title>          </block>        </value>        <next>          <block type="inout_buildin_led">            <title name="STAT">LOW</title>            <next>              <block type="base_delay" inline="true">                <value name="DELAY_TIME">                  <block type="math_number">                    <title name="NUM">200</title>                  </block>                </value>              </block>            </next>          </block>        </next>      </block>    </next>  </block></xml>'
      var dom = Blockly.Xml.textToDom(xml)
      Blockly.mainWorkspace.clear();
      Blockly.Xml.domToWorkspace(Blockly.mainWorkspace, dom); 

  }
};


Blockly.Language.sound_light = {
  category: 'Som agudo/grave',
  init: function() {
      trigger_click('#sound_light')
      var xml = '<xml xmlns="http://www.w3.org/1999/xhtml">  <block type="base_delay" inline="true" x="7" y="-27">    <value name="DELAY_TIME">      <block type="math_number">        <title name="NUM">5</title>      </block>    </value>    <next>      <block type="variables_declare" inline="false">        <title name="VAR">pitch</title>        <title name="TYPE">int</title>        <value name="VALUE">          <block type="math_number">            <title name="NUM">0</title>          </block>        </value>        <next>          <block type="variables_set" inline="false">            <title name="VAR">pitch</title>            <value name="VALUE">              <block type="map_easyduino" inline="false">                <title name="map_2">440</title>                <title name="map_3">800</title>                <title name="map_4">1000</title>                <title name="map_5">5500</title>                <value name="map_1">                  <block type="inout_analog_read">                    <title name="PIN">A0</title>                  </block>                </value>              </block>            </value>            <next>              <block type="tone_easyduino" inline="true">                <value name="write">                  <block type="math_number">                    <title name="NUM">10</title>                  </block>                </value>                <value name="value">                  <block type="variables_get">                    <title name="VAR">pitch</title>                  </block>                </value>                <value name="duration">                  <block type="math_number">                    <title name="NUM">5</title>                  </block>                </value>              </block>            </next>          </block>        </next>      </block>    </next>  </block></xml>'
      var dom = Blockly.Xml.textToDom(xml)
      Blockly.mainWorkspace.clear();
      Blockly.Xml.domToWorkspace(Blockly.mainWorkspace, dom); 

  }
};

Blockly.Language.sound_light_ext = {
  category: 'Led externo',
  init: function() {
      trigger_click('#sound_light_ext')
      var xml = '<xml xmlns="http://www.w3.org/1999/xhtml">  <block type="variables_declare" inline="false" x="61" y="-79">    <title name="VAR">led</title>    <title name="TYPE">int</title>    <value name="VALUE">      <block type="math_number">        <title name="NUM">9</title>      </block>    </value>    <next>      <block type="variables_declare" inline="false">        <title name="VAR">fadeAmount</title>        <title name="TYPE">int</title>        <value name="VALUE">          <block type="math_number">            <title name="NUM">5</title>          </block>        </value>        <next>          <block type="variables_declare" inline="false">            <title name="VAR">brightness</title>            <title name="TYPE">int</title>            <value name="VALUE">              <block type="math_number">                <title name="NUM">0</title>              </block>            </value>            <next>              <block type="pinmodeout_easyduino" inline="true">                <value name="write">                  <block type="variables_get">                    <title name="VAR">led</title>                  </block>                </value>                <next>                  <block type="analogwrite_easyduino" inline="true">                    <value name="write">                      <block type="variables_get">                        <title name="VAR">led</title>                      </block>                    </value>                    <value name="value">                      <block type="variables_get">                        <title name="VAR">brightness</title>                      </block>                    </value>                    <next>                      <block type="variables_set" inline="false">                        <title name="VAR">brightness</title>                        <value name="VALUE">                          <block type="math_arithmetic" inline="true">                            <title name="OP">ADD</title>                            <value name="A">                              <block type="variables_get">                                <title name="VAR">brightness</title>                              </block>                            </value>                            <value name="B">                              <block type="variables_get">                                <title name="VAR">fadeAmount</title>                              </block>                            </value>                          </block>                        </value>                        <next>                          <block type="controls_if" inline="false">                            <value name="IF0">                              <block type="logic_compare" inline="true">                                <title name="OP">EQ</title>                                <value name="A">                                  <block type="variables_get">                                    <title name="VAR">brightness</title>                                  </block>                                </value>                                <value name="B">                                  <block type="math_number">                                    <title name="NUM">0</title>                                  </block>                                </value>                              </block>                            </value>                            <statement name="DO0">                              <block type="variables_set" inline="false">                                <title name="VAR">fadeAmount</title>                                <value name="VALUE">                                  <block type="math_arithmetic" inline="true">                                    <title name="OP">MINUS</title>                                    <value name="A">                                      <block type="math_number">                                        <title name="NUM">0</title>                                      </block>                                    </value>                                    <value name="B">                                      <block type="variables_get">                                        <title name="VAR">fadeAmount</title>                                      </block>                                    </value>                                  </block>                                </value>                              </block>                            </statement>                            <next>                              <block type="controls_if" inline="false">                                <value name="IF0">                                  <block type="logic_compare" inline="true">                                    <title name="OP">EQ</title>                                    <value name="A">                                      <block type="variables_get">                                        <title name="VAR">brightness</title>                                      </block>                                    </value>                                    <value name="B">                                      <block type="math_number">                                        <title name="NUM">155</title>                                      </block>                                    </value>                                  </block>                                </value>                                <statement name="DO0">                                  <block type="variables_set" inline="false">                                    <title name="VAR">fadeAmount</title>                                    <value name="VALUE">                                      <block type="math_arithmetic" inline="true">                                        <title name="OP">MINUS</title>                                        <value name="A">                                          <block type="math_number">                                            <title name="NUM">0</title>                                          </block>                                        </value>                                        <value name="B">                                          <block type="variables_get">                                            <title name="VAR">fadeAmount</title>                                          </block>                                        </value>                                      </block>                                    </value>                                  </block>                                </statement>                                <next>                                  <block type="base_delay" inline="true">                                    <value name="DELAY_TIME">                                      <block type="math_number">                                        <title name="NUM">30</title>                                      </block>                                    </value>                                  </block>                                </next>                              </block>                            </next>                          </block>                        </next>                      </block>                    </next>                  </block>                </next>              </block>            </next>          </block>        </next>      </block>    </next>  </block></xml>'
      var dom = Blockly.Xml.textToDom(xml)
      Blockly.mainWorkspace.clear();
      Blockly.Xml.domToWorkspace(Blockly.mainWorkspace, dom); 
  }
};


Blockly.Language.map_easyduino = {
  category: 'Matemática',
  init: function() {
    this.setColour(120);
    this.appendValueInput('map_1')
        .appendTitle("Map EasyDuino | Endereço")
    this.appendDummyInput("")
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendTitle("Map 2")
        .appendTitle(new Blockly.FieldTextInput("440"), "map_2");
    this.appendDummyInput("")
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendTitle("Map 3")
        .appendTitle(new Blockly.FieldTextInput("800"), "map_3");
    this.appendDummyInput("")
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendTitle("Map 4")
        .appendTitle(new Blockly.FieldTextInput("1000"), "map_4");
    this.appendDummyInput("")
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendTitle("Map 5")
        .appendTitle(new Blockly.FieldTextInput("5500"), "map_5");
    this.setTooltip('');
    //this.setInputsInline(true);
    //this.setPreviousStatement(true);
    this.setOutput(true);
    //this.setNextStatement(true);
  }
};

Blockly.Arduino.map_easyduino = function() {
  var map_1 = Blockly.Arduino.valueToCode(this, 'map_1', Blockly.Arduino.ORDER_ATOMIC);
  var map_2 = this.getTitleValue('map_2');
  var map_3 = this.getTitleValue('map_3');
  var map_4 = this.getTitleValue('map_4');
  var map_5 = this.getTitleValue('map_5');
  var code = 'map(' + map_1 + ', ' + map_2 + ', ' + map_3 + ', ' + map_4 + ', ' + map_5 + ')'
  return [code, Blockly.Arduino.ORDER_ATOMIC]
};



Blockly.Language.tone_easyduino = {
  category: 'Matemática',
  init: function() {
    this.setColour(120);
    this.appendValueInput('write')
        .appendTitle("Tone EasyDuino | Endereço")
    this.appendValueInput('value')
        .appendTitle("Valor")
    this.appendValueInput('duration')
        .appendTitle("Duração")
    this.setTooltip('');
    this.setInputsInline(true);
    this.setPreviousStatement(true);
    //this.setNextStatement(true);
  }
};

Blockly.Arduino.tone_easyduino = function() {
  var write = Blockly.Arduino.valueToCode(this, 'write', Blockly.Arduino.ORDER_ATOMIC);
  var duration = Blockly.Arduino.valueToCode(this, 'duration', Blockly.Arduino.ORDER_ATOMIC);
  var value = Blockly.Arduino.valueToCode(this, 'value', Blockly.Arduino.ORDER_ATOMIC);
  var code = 'tone(' + write + ',' + value + ', ' + duration + ');\n'
  return code
};

Blockly.Language.pinmodeout_easyduino = {
  category: 'Entrada/Saída',
  init: function() {
    this.setColour(120);
    this.appendValueInput('write')
        .appendTitle("PinModeOut EasyDuino | Led")
    this.setTooltip('');
    this.setInputsInline(true);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
  }
};

Blockly.Arduino.pinmodeout_easyduino = function() {
  var write = Blockly.Arduino.valueToCode(this, 'write', Blockly.Arduino.ORDER_ATOMIC);
  var code = 'pinMode(' + write + ',OUTPUT);\n'
  return code
};


Blockly.Language.analogwrite_easyduino = {
  category: 'Entrada/Saída',
  init: function() {
    this.setColour(120);
    this.appendValueInput('write')
        .appendTitle("AnalogWrite EasyDuino | Led")
    this.appendValueInput('value')
        .appendTitle("Value")
    this.setTooltip('');
    this.setInputsInline(true);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
  }
};

Blockly.Arduino.analogwrite_easyduino = function() {
  var write = Blockly.Arduino.valueToCode(this, 'write', Blockly.Arduino.ORDER_ATOMIC);
  var value = Blockly.Arduino.valueToCode(this, 'value', Blockly.Arduino.ORDER_ATOMIC);
  var code = 'analogWrite(' + write + ', ' + value + ');\n'    
  return code
};

// define generators


/*

Led code:
Blink
Turns on an LED on for one second, then off for one second, repeatedly.
 
This example code is in the public domain.

 
// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
int led = 13;

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(led, OUTPUT);     
}

// the loop routine runs over and over again forever:
void loop() {
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);               // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);               // wait for a second
}

*/